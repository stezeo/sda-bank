package com.sda.bank.authentication;

import com.sda.bank.model.User;

public interface AuthenticationServiceFactory {

    void createUser(final User newUser);

    User login(final User user);

}
