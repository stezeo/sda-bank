package com.sda.bank.authentication;

import com.sda.bank.model.FileOperation;
import com.sda.bank.model.User;
import lombok.Data;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

@Data
public class FileAuthenticationService implements AuthenticationServiceFactory {

    private final FileOperation fileOperation = new FileOperation();

    /**
     * todo
     * ask if new or existing user
     * request details
     * user enters login details
     * checks list of users against details entered
     * throw 2 exeptions- worng username, wrong password
     * when correct, opens users profile - for now, perhaps display currency available and the option to send remitance transfer
     * user perhaps has 2 different sets of details- 1st is personal/ profile info - DOB, address etc, 2nd is bank balances, transaction history
     */

    @Override
    public void createUser(final User newUser) {
        try {
            fileOperation.open("resource/user.txt");

            final List<User> users = fileOperation.getUsers();
            final boolean isUserExist = users.stream().anyMatch(user -> user.getEmailAddress().equals(newUser.getEmailAddress()));
            if (!isUserExist) {
                fileOperation.writeObjectToFile(newUser);
            } else {
                System.out.println("You're already member with this email account:" + newUser.getEmailAddress());
                System.out.println("Please try to login");
            }

        } catch (IOException ex) {
            System.out.println("We can not save your new record");
            ex.printStackTrace();
        }
    }

    @Override
    public User login(final User user) {
        fileOperation.open("resource/user.txt");
        final List<User> users = fileOperation.getUsers();
        final Optional<User> authenticatedUserOptional = users.stream().
                filter(authenticatedUser -> authenticatedUser.getEmailAddress().equals(user.getEmailAddress()) && authenticatedUser.getPassword().equals(user.getPassword())).findFirst();
        return authenticatedUserOptional.isPresent() ? authenticatedUserOptional.get() : null;
    }

}
