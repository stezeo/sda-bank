package com.sda.bank.model;

import lombok.SneakyThrows;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class FileOperation {

    private File file;

    public void open(String path) {
        this.file = new File(path);

        System.out.println("File Path:" + file.getPath());
        System.out.println("Absolute Path:" + file.getAbsoluteFile());
        System.out.println("File Name:" + file.getName());
        System.out.println("Is file:" + file.isFile());
    }

    public void writeObjectToFile(User user) throws IOException {
        List<User> users = getUsers();
        users.add(user);

        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);

        objectOutputStream.writeObject(users);

        fileOutputStream.close();
        objectOutputStream.close();
    }

    @SneakyThrows
    private List<User> readObjectsFromFile() {
        List<User> users = new ArrayList<>();
        FileInputStream fileInputStream = new FileInputStream(file);
        if (fileInputStream.available() != 0) {
            User user;
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            while (fileInputStream.available() > 0) {
                users = (List<User>) objectInputStream.readObject();
                //users.add(user);
            }
            objectInputStream.close();
        }
        fileInputStream.close();
        return users;
    }


    public List<User> getUsers() {
        return readObjectsFromFile();
    }


}
