package com.sda.bank.model;

public enum UserType {
    PRIVATE,
    BUSINESS
}
