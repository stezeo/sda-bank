package com.sda.bank;

import com.sda.bank.authentication.AuthenticationServiceFactory;
import com.sda.bank.authentication.FileAuthenticationService;
import com.sda.bank.model.User;
import com.sda.bank.model.UserType;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

public class SdaBank {

    public static void main(String[] args) throws NoSuchAlgorithmException {
        System.out.println("Welcome to SDA Bank");
        Scanner scanner = new Scanner(System.in);
        int selection = 0;
        do {
            System.out.println("New to SDA Bank? Chose option:1");
            System.out.println("If you have account, Please chose option:2");
            System.out.println("If you want to exit, Please chose option:9");
            selection = scanner.nextInt();
            if (selection == 1) {
                doSignUp();
            } else if (selection == 2) {
                doLogin();
            }
        } while (selection != 9);

        System.out.println("Thanks for using us. Good bye!");
    }

    private static void doSignUp() throws NoSuchAlgorithmException {
        User user = new User();
        Scanner scanner = new Scanner(System.in);
        System.out.println("#### SDA Bank User Sign up Page ###");
        System.out.println("Please select your  customer type");
        System.out.println("Select 1 for PRIVATE Customer");
        System.out.println("Select 2 for BUSINESS Customer");
        final int customerTypeOption = scanner.nextInt();
        if (customerTypeOption == 1) {
            user.setUserType(UserType.PRIVATE);
        } else if (customerTypeOption == 2) {
            user.setUserType(UserType.BUSINESS);
        }

        System.out.println("Please enter your  email address");
        user.setEmailAddress(scanner.next());

        System.out.println("Please enter your  password");
        String userPassword = scanner.next();
        user.setPassword(getMd5Password(userPassword));

        System.out.println("Please enter country of residence");
        user.setCountryOfResidence(scanner.next());

        final AuthenticationServiceFactory authenticationServiceFactory = new FileAuthenticationService();
        authenticationServiceFactory.createUser(user);

        doLogin();
    }

    private static void doLogin() throws NoSuchAlgorithmException {
        int incorrectLoginAttempt = 0;
        System.out.println("#### SDA Bank Login Page ###");

        do {
            Scanner scanner = new Scanner(System.in);
            User user = new User();
            System.out.println("Please enter your  email address");
            user.setEmailAddress(scanner.nextLine());

            System.out.println("Please enter your password");
            String userPassword = scanner.next();
            user.setPassword(getMd5Password(userPassword));

            final AuthenticationServiceFactory authenticationServiceFactory = new FileAuthenticationService();
            final User login = authenticationServiceFactory.login(user);
            if (login != null) {
                System.out.println("You are logged in! Welcome, " + login.getEmailAddress());
            } else {
                incorrectLoginAttempt++;
                System.out.println("Username or password is incorrect");
            }
        } while (incorrectLoginAttempt < 3);

        if (incorrectLoginAttempt >= 3) {
            System.out.println("You have attempted to enter 3 times incorrect login credentials.");
            System.out.println(" ### Your account has been blocked. Please contact with customer service ###");
        }
    }

    private static String getMd5Password(final String password) {
        try {
            // Create MessageDigest instance for MD5
            MessageDigest md = MessageDigest.getInstance("MD5");
            //Add password bytes to digest
            md.update(password.getBytes());
            //Get the hash's bytes
            byte[] bytes = md.digest();
            //This bytes[] has bytes in decimal format;
            //Convert it to hexadecimal format
            final StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            //Get complete hashed password in hex format
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
